const functions = require("firebase-functions");

const admin = require("firebase-admin");

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: "https://proyecto-bd-dca35-default-rtdb.firebaseio.com/",
});

const db = admin.firestore();
exports.onUserCreate = functions.firestore
    .document("reserva/{userId}")
    .onCreate(async (snap, context) => {

        const valores = snap.data();

        // PRIMERA CONSULTA SOBRE LA CANTIDAD DE EVENTOS ADMINISTRADOS
        let query2 = db.collection("reserva").where("emp_id", "==", valores.emp_id);
        await query2.get().then((snap) => {
            size = snap.size;
        });
        //CONDICION QUE DICTAMINA SI SI SE APLICA EL AUMENTO SALARIAL O NO
        if(size < 4) {
            //EN CASO DE QUE NO SE EJECUTE SE ENVIARA UN MENSAJE QUE INDICA QUE NO SE REALIZO EL AUMENTO
            console.log("No aplica para Aumento salarial");
        }
        else {
            //EN EL CASO DE QUE SI SE PORCEDE CON EL SIGUEINTE PROCESO
            const agentes = await db
                .collection("empleado")
                .where("emp_id", "==", valores.emp_id);
            const item = await agentes.get();
            let var1 = 0;
            item.forEach(
                (querySnapshot) =>
                    var1 =
                        //AQUI OBTENEMOS LAS VARIABLES Y REALIZAMOS EL CALCULO DEL AUMENTO PARA LUEGO
                        //GUARDARLO EN LA VARIABLE VAR1
                        (querySnapshot.data().emp_sueldo_mensual * 0.2) +
                        querySnapshot.data().emp_sueldo_mensual
            );
            //POSRTERIOR PROCEDEMOS A ACTUALIZAR EL CAMPO DICTAMINADO PARA APLICAR EL AUMENTO
            const snapshot = await db
                .collection("empleado")
                .where("emp_id", "==", valores.emp_id)
                .get();
            let updatePromises = [];
            snapshot.forEach((doc) => {
                updatePromises.push(
                    db
                        .collection("empleado")
                        .doc(doc.id)
                        .update({ emp_sueldo_mensual: var1 })
                );
            }); //COMO ULTIMO PUNTO SE EJECUTARIA UN MESNAJE EN 
                //CONSOLA QUE INDICA QUE SE LE REALIZO EL 20% DE AUMENTO SALARIAL
            await Promise.all(updatePromises);
            console.log(`Empleado #${valores.emp_id}Se le subio el sueldo un 20%`);

        }

    } )